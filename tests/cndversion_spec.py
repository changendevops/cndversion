import sys
import os
sys.path.append('lib')
from mockito import when, mock, unstub
from expects import *
from mamba import description, context, it
import cndversion

with description("CndVersion"):
    with before.each:
        unstub()
        args = mock(
            {
                "folder": '.testfiles'
            }
        )
        self.cnd_version = cndversion.CndVersion(args)

    with context("_build_init"):
        with it("should create init file"):
            filename = '.testfiles/__init__.py'
            if os.path.exists(filename) is True:
                os.remove(filename)
            result = self.cnd_version._build_init('.testfiles')
            expected_content = open(filename).read()
            real_content = open('.testvalues/init_empty.py').read()
            expect(real_content).to(equal(expected_content))

        with it("should update init file"):
            open(".testfiles/__init__.py", 'w').write("abc\n")
            result = self.cnd_version._build_init('.testfiles')
            expected_content = open('.testfiles/__init__.py').read()
            real_content = open('.testvalues/init_with_content.py').read()
            expect(real_content).to(equal(expected_content))